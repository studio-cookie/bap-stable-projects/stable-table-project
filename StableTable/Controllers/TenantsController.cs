﻿using Core.Interfaces;
using Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace StableTable.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TenantsController : ControllerBase
    {
        private readonly ITenantService _service;
        public TenantsController(ITenantService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> CreateTenantAsync(Tenant tenant)
        {
            try
            {
                return Ok(await _service.CreateAsync(tenant));
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
