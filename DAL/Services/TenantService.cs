﻿using Core.Interfaces;
using Core.Models;
using DAL.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class TenantService : ITenantService
    {
        private readonly TenantDbContext _context;
        public TenantService(TenantDbContext context)
        {
            _context = context;
        }
        public async Task<Tenant> CreateAsync(Tenant tenant)
        {
            _context.Tenants.Add(tenant);
            await _context.SaveChangesAsync();
            return tenant;
        }
    }
}
