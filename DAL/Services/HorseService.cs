﻿using Core.Interfaces;
using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class HorseService : IHorseService
    {
        private readonly ApplicationDbContext _context;
        public HorseService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Horse> CreateAsync(string name)
        {
            Horse horse = new Horse(name);
            _context.Horses.Add(horse);
            await _context.SaveChangesAsync();
            return horse;
        }
        public async Task<IReadOnlyList<Horse>> GetAllAsync()
        {
            return await _context.Horses.ToListAsync();
        }
        public async Task<Horse> GetByIdAsync(int id)
        {
            return await _context.Horses.FindAsync(id);
        }
    }
}
