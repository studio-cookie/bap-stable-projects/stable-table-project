﻿using Core.Interfaces;
using Core.Models;
using DAL.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class AuthService : IAuthService
    {
        private Tenant _currentTenant;
        private HttpContext _httpContext;
        private TenantDbContext _context;

        public AuthService(IHttpContextAccessor contextAccessor, TenantDbContext context)
        {
            _httpContext = contextAccessor.HttpContext;
            _context = context;

            if (_httpContext != null)
            {
                if (_httpContext.Request.Headers.TryGetValue("tenant", out var tenantId))
                {
                    SetTenant(Int32.Parse(tenantId[0]));
                }
                else
                {
                    throw new Exception("Invalid Tenant!");
                }
            }
        }

        private void SetTenant(int tenantId)
        {
            _currentTenant = _context.Tenants.Where(a => a.Id == tenantId).FirstOrDefault();
            if (_currentTenant == null) throw new Exception("Invalid Tenant!");
        }


        public Tenant GetTenant()
        {
            return _currentTenant;
        }
    }
}
