﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface IMustHaveTenant
    {
        public int TenantId { get; set; }
    }
}
