﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IHorseService
    {
        Task<Horse> CreateAsync(string name);
        Task<Horse> GetByIdAsync(int id);
        Task<IReadOnlyList<Horse>> GetAllAsync();
    }
}
