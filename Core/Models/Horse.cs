﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Horse : BaseEntity, IMustHaveTenant
    {
        public Horse(string name)
        {
            Name = name;
        }
        protected Horse()
        {
        }
        public string Name { get; private set; }

        public int TenantId { get; set; }
    }
}
