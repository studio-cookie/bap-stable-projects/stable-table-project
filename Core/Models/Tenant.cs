﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Models
{
    public class Tenant: BaseEntity
    {
        public Tenant(string name)
        {
            Name = name;
        }
        protected Tenant()
        {
        }
        public string Name { get; set; }
        
    }
}
